const { spawn } = require('child_process')
const { promisify } = require('util')
const { join, dirname } = require('path')
const { createWriteStream, readFile } = require('fs')
const mkdirp = require('mkdirp')
const { extract } = require('tar-stream')
const h = require('hastscript')
const unified = require('unified')
const remarkParse = require('remark-parse')
const remarkHighlight = require('remark-highlight.js')
const remarkToc = require('remark-toc')
const remarkRehype = require('remark-rehype')
const remarkSlug = require('remark-slug')
const rehypeFormat = require('rehype-format')
const rehypeAutolinkHeadings = require('rehype-autolink-headings')
const rehypeStringify = require('rehype-stringify')
const hastUtilSelect = require('hast-util-select')
const hastUtilHeading = require('hast-util-heading')
const hastUtilToString = require('hast-util-to-string')
const unistUtilMap = require('unist-util-map')
const unistUtilFind = require('unist-util-find')
const unistUtilVisit = require('unist-util-visit')
const unistRemove = require('unist-util-remove')
const micromatch = require('micromatch')
const yauzl = require('yauzl')
const fetch = require('node-fetch')
const { iterator: pIterator } = require('p-event')

const repositories = [
  { git: 'git@gitlab.com:commonshost/cli.git' },
  { zip: 'https://github.com/commonshost/dohnut/archive/master.zip' },
  { zip: 'https://github.com/commonshost/bulldohzer/archive/master.zip' },
  { git: 'git@gitlab.com:commonshost/server.git' },
  { git: 'git@gitlab.com:commonshost/manifest.git' },
  { git: 'git@gitlab.com:commonshost/core.git' }

  // { git: 'file:///Users/sebdeckers/code/commonshost/cli' },
  // { git: 'file:///Users/sebdeckers/code/commonshost/dohnut' },
  // { git: 'file:///Users/sebdeckers/code/commonshost/bulldohzer' },
  // { git: 'file:///Users/sebdeckers/code/commonshost/server' },
  // { git: 'file:///Users/sebdeckers/code/commonshost/manifest' },
  // { git: 'file:///Users/sebdeckers/code/commonshost/core' }
]

async function getProjectFiles (repository) {
  const paths = ['README.md', 'docs/**/*.{md,png}', '*.png']
  if (repository.zip) {
    return downloadZip(repository, paths)
  } else if (repository.git) {
    return gitArchiveRemote(repository, paths)
  }
}

async function downloadZip ({ zip }, paths) {
  const files = []
  let root
  const response = await fetch(zip)
  const archive = await response.buffer()
  const zipfile = await promisify(yauzl.fromBuffer).bind(yauzl)(
    archive,
    { lazyEntries: true }
  )
  const entries = pIterator(zipfile, 'entry', { resolutionEvents: ['end'] })
  zipfile.readEntry()
  for await (const entry of entries) {
    if (entry.fileName.endsWith('/')) {
      if (root === undefined) root = entry.fileName
      zipfile.readEntry()
    } else {
      const source = entry.fileName.substr(root.length)
      if (!micromatch.some(source, paths)) {
        zipfile.readEntry()
      } else {
        const openReadStream = promisify(zipfile.openReadStream).bind(zipfile)
        const readStream = await openReadStream(entry)
        const file = { source }
        const chunks = []
        for await (const chunk of readStream) {
          chunks.push(chunk)
        }
        file.raw = Buffer.concat(chunks)
        files.push(file)
        zipfile.readEntry()
      }
    }
  }
  return files
}

async function gitArchiveRemote ({ git, branch = 'HEAD' }, paths) {
  const untar = extract()
  const command = ['archive', '--remote', git, branch]
  const child = spawn('git', command)
  child.stderr.pipe(process.stderr)
  child.stdout.pipe(untar)
  const files = []
  const entries = pIterator(untar, 'entry', {
    multiArgs: true,
    resolutionEvents: ['finish']
  })
  for await (const [header, stream, next] of entries) {
    if (header.type !== 'file') next()
    else if (!micromatch.some(header.name, paths)) next()
    else {
      const buffers = []
      const chunks = pIterator(stream, 'data', { resolutionEvents: ['end'] })
      for await (const chunk of chunks) {
        buffers.push(chunk)
      }
      files.push({
        source: header.name,
        raw: Buffer.concat(buffers)
      })
      next()
    }
  }
  return files
}

function fileDestination (file) {
  const prefix = 'docs/'
  let destination = file.source
    .replace(/README\.md$/i, 'index.html')
    .replace(/\.md$/i, '.html')
  if (destination.startsWith(prefix)) {
    destination = destination.substr(prefix.length)
  }
  return { ...file, destination }
}

function filePath (file) {
  const path = file.destination.replace(/index.html$/, '')
  return { ...file, path }
}

async function processMarkdown (file) {
  if (!file.source.match(/\.md$/i)) {
    return file
  }
  let toc, title
  const processor = unified()
    .use(remarkParse)
    .use(remarkHighlight)
    .use(remarkSlug)
    .use((options) => (node) => {
      unistUtilVisit(node, 'link', (link) => {
        if (link.url.startsWith('./docs/')) {
          link.url = link.url.replace('./docs/', './')
        }
      })
    })
    .use(remarkToc, { maxDepth: 2 })
    .use(remarkRehype)
    .use((options) => (node) => {
      const codeblocks = 'pre > code:not(.hljs)'
      for (const codeblock of hastUtilSelect.selectAll(codeblocks, node)) {
        if (!('className' in codeblock.properties)) {
          codeblock.properties.className = []
        }
        if (!codeblock.properties.className.includes('hljs')) {
          codeblock.properties.className.push('hljs')
        }
      }
    })
    .use(rehypeAutolinkHeadings)
    .use((options) => (node) => {
      const heading = hastUtilSelect.select('#table-of-contents', node)
      const list = hastUtilSelect.select('#table-of-contents ~ ul', node)
      if (file.destination === 'index.html') {
        unistRemove(node, (child) => child === heading)
        unistRemove(node, (child) => child === list)
      }
      toc = list
    })
    .use((options) => (node) => {
      const heading = unistUtilFind(node, hastUtilHeading)
      title = hastUtilToString(heading)
    })
  const markdown = file.raw.toString()
    .replace('\n## ', '\n## Table of Contents\n\n## ')
  const tree = await processor.parse(markdown)
  const hast = await processor.run(tree)
  return { toc, title, hast, ...file }
}

function getNavigation (project) {
  return h('ul', project.files
    .filter(({ source }) => source.endsWith('.md'))
    .filter(({ destination }) => destination !== 'index.html')
    .map((file) => file.toc && h('li', [
      h('a', { href: `/${project.path}/${file.path}` }, file.title),
      unistUtilMap(file.toc, (node, index, parent) => {
        if (node.type === 'element' && node.tagName === 'a') {
          return {
            ...node,
            properties: {
              ...node.properties,
              href: `/${project.path}/${file.path}${node.properties.href}`
            }
          }
        }
        return node
      })
    ]))
    .filter(Boolean)
  )
}

async function getProjects (repositories) {
  let projects = repositories
    .map((repository) => ({ repository }))
  projects = projects.map((project) => {
    let path
    if (project.repository.git) {
      path = project.repository.git
        .match(/\/([\w\d]+)(\.git)?$/)[1]
    } else if (project.repository.zip) {
      path = project.repository.zip
        .match(/\/([\w\d]+)\/archive\/master.zip$/)[1]
    }
    return { ...project, path }
  })
  projects = await Promise.all(projects.map(async (project) => {
    let files = await getProjectFiles(project.repository)
    if (files.length === 0) throw new Error('Failed to load files.')
    files = files.map(fileDestination)
    files = files.map(filePath)
    files = await Promise.all(files.map(processMarkdown))
    return { ...project, files }
  }))
  projects = projects.map((project) => {
    const toc = getNavigation(project)
    const index = project.files
      .find(({ destination }) => destination === 'index.html')
    index.hast.children.push(
      h('h2', { id: 'table-of-contents' }, [
        h('a', { 'aria-hidden': 'true', href: '#table-of-contents' }, [
          h('span', { class: 'icon icon-link' })
        ]),
        'Table of Contents'
      ]),
      toc
    )
    const { title } = index
    return { ...project, title }
  })
  return projects
}

async function hastToHtml (node) {
  const processor = await unified()
    .use(rehypeFormat)
    .use(rehypeStringify)
  const formatted = await processor.run(node)
  const html = await processor.stringify(formatted)
  return html
}

async function getOverview () {
  const raw = await promisify(readFile)(join(__dirname, 'README.md'), 'utf8')
  const file = await processMarkdown({
    source: 'README.md',
    destination: 'index.html',
    title: 'Overview',
    raw: raw.replace(/^# .+$/m, '# Overview\n')
  })
  return file
}

async function main () {
  const projects = await getProjects(repositories)
  const navigation = await hastToHtml(
    h('ul', projects.map((project) => h('li', [
      h('a', { href: `/${project.path}/` }, project.title.split(' ')[0]),
      getNavigation(project)
    ])))
  )
  const templates = {
    page: await promisify(readFile)(join(__dirname, 'templates/page.html'), 'utf8')
  }

  const overview = await getOverview()
  overview.hast.children.push(
    h('ul', { id: 'projects-overview' }, projects.map((project) => {
      return h('li', { class: project.path }, [
        h('a', { href: `/${project.path}/` }, project.title)
      ])
    }))
  )
  projects.push({
    url: 'https://commons.host',
    path: '',
    title: 'Commons Host',
    files: [overview]
  })

  for (const project of projects) {
    for (const file of project.files) {
      const base = join(process.cwd(), 'public')
      const filepath = join(base, project.path, file.destination)
      console.log('--', filepath)
      await promisify(mkdirp)(dirname(filepath))
      const output = createWriteStream(filepath)
      if (file.hast) {
        const content = await hastToHtml(file.hast)
        const title = file.title === project.title || !project.title
          ? file.title : `${file.title} - ${project.title}`
        const substitutions = {
          'page.content': content,
          'page.title': title,
          'project.path': project.url || `/${project.path}/`,
          'project.title': project.title,
          'site.navigation': navigation
        }
        const html = templates.page.replace(
          /{{\s*([-.\w]+)\s*}}/g,
          (match, p1) => substitutions[p1]
        )
        output.write(html)
      } else {
        output.write(file.raw)
      }
      output.end()
    }
  }
}

main()
